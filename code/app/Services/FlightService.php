<?php
    namespace App\Services;
    use Illuminate\Http\Request;
    use App\Repositories\FlightRepository;

    class FlightService{
        public function __construct(FlightRepository $flightrepository)
        {
            $this->flightrepository = $flightrepository;
        }
        public function create($request)
        {
            $flight = $this->flightrepository->create($request);
            return $flight;
        }
        public function findById($id)
        {
            $flight = $this->flightrepository->findById($id);
            return $flight;
        }

        public function updateProduct($request, $id)
        {
            $flight = $this->flightrepository->updatePro($request, $id);
            return $flight;
        }

        public function delete($id)
        {
            $flight = $this->flightrepository->deletePro($id);
            return $flight;
        }
    }
?>