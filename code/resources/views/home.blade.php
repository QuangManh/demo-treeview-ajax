<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <h1>Xin chào: {{ auth()->guard('admin')->user()->name_admin }}<a href="{{ route('logout') }}">Đăng xuất</a></h1>
    @if (auth()->guard('admin')->user()->position == 1)
        <a href="{{ route('create') }}" class="btn btn-primary">Thêm</a>
    @endif
    <h5>Chức vụ:
        @if (auth()->guard('admin')->user()->position == 1)
            Admin
        @else
            Người dùng
        @endif
    </h5>
    <a href="{{ route('send_mail') }}">Gửi mail</a>

    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th>Tên</th>
                    <th>Giá</th>
                    <th>Ảnh</th>
                    <th>Mô tả</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pro as $item)
                    <tr>
                        <td scope="row">{{ $item->name }}</td>
                        <td>{{ $item->price }}</td>
                        <td><img src="{{ FILE_ROOT }}{{ $item->img }}" width="70px" alt=""></td>
                        <td>{{ $item->describe }}</td>
                        @if (auth()->guard('admin')->user()->position == 1)
                            <td><a href="{{ route('update', ['id' => $item->id]) }}" class="btn btn-success ">Sửa</a>
                            </td>
                            <td><a href="{{ route('delete', ['id' => $item->id]) }}" class="btn btn-danger ">Xóa</a>
                            </td>
                        @endif

                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</body>

</html>
