<?php
    namespace App\Repositories;
    use App\Models\UserTest;
    class UserTestRepository{
        public function __construct(UserTest $usertest)
        {
            $this->usertest = $usertest;
        }

        public function addUser($request)
        {
            return $this->usertest->create($request);
        }
    }
?>