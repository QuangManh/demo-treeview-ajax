<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;

class LoginRequest extends FormRequest
{
    // public $validator = null;
    // /**
    //  *
    //  * Check the validation is fails
    //  * @return validator
    //  */
    // protected function failedValidation(Validator $validator)
    // {
    //     $this->validator = $validator;
    // }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required | email:rfc,dns',
            'password' => 'required|check_password_format',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Không được để trống email',
            'email.email' => 'Định dạng email chưa đúng',
            'password.check_password_format' => 'chữ cái đầu phải in hoa',
            'password.required' => 'Không được để trống mật khẩu',
        ];
    }
}
